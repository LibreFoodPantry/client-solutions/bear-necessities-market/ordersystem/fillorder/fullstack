## 'npm install' Details

The 'npm install' command is used to install dependencies to a node project. The first important thing to note about the command is that it has an option to install dependencies **locally** or **globally** using the -g flag.

### Example of Usage:
```
npm install -g <package-name>
```

**Local Installation**: The dependency will be installed in the working directory of the given Node project, within the 'node_modules' folder. This is the default when the -g flag is not present.

**Global Installation**: The dependency will be installed at a global scope accessible by all Node projects on the computer. Any executable scripts within the dependency will also be available to use on the command line.

In general, dependencies should be installed globally **only when they contain scripts to be used on the command line**. For example, the **npm** package is installed globally, which makes sense, because it is run from the command line.

## Package.json: _dependencies_ vs. _devDependencies_
When 'npm install' is run, the installed dependency will be added automatically in the package.json file as a dependency of the project.

There are two spots that dependencies can live:
1. In the **'dependency'** attribute of package.json
2. In the **'devDependency'** attribute of package.json

**dependency**: Any dependencies in here are for the production version of the project. When the project is installed on a server or elsewhere, any dependencies here will be required for it to run.

**devDependency**: Any dependencies in here are strictly for development of the project. These will not be included in the final production version of the project. Examples would be testing frameworks or code minifying tools.

Within each of these attributes, the version of the dependency can be specified.
Example:
```JSON
"dependencies": {
	"random_package": "2.1.2"
}
```

To add a dependency as a development dependency, use the --save-dev flag. When the flag is not specified, the dependency is installed as a production dependency.

For details on specifying versions, follow this link: [Semantic versioning in npm]([https://docs.npmjs.com/misc/semver](https://docs.npmjs.com/misc/semver))

Other resources with additional details:
* ['npm install' documentation](https://docs.npmjs.com/cli/install)
* [npm scope documentation](https://docs.npmjs.com/misc/scope)