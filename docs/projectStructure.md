
## Project Structure
To start, here are some great resources detailing robust structures for Node projects:
* [Structure of a NodeJS API Project](https://medium.com/codebase/structure-of-a-nodejs-api-project-cdecb46ef3f8)
* [Bulletproof node.js project architecture](https://softwareontheroad.com/ideal-nodejs-project-structure/)

The rest of this file will detail the project structure for FillOrder

## Backend
### Important Info
##### --REST APIS--
FillOrder uses **REST API** endpoints for Frontend-to-Backend communication, and based on most project structures that were researched, a common practice when it comes to REST APIs is keeping the **endpoint declarations (_routes_)** and the **functionality provided by those endpoints (_controllers_)** in separate folders.

This is because there are often numerous endpoints for a given aspect of the project, so putting both the endpoints and their respective functionality in one file would make a very long, ugly file. Keeping them separate helps keep the project and code clean.

There are also what are called **middleware** functions, which are functions that can be created in **express.js** that lie between **routes** and **controllers**. They provide functionality that alters requests as they come in to provide more properties that can be used in the controllers.

* Here is a good guide regarding **middleware**:  [Guide to middleware in Express](https://expressjs.com/en/guide/writing-middleware.html)

Finally, there are **services**, which essentially serve to handle complicated business logic that should not be crammed into controllers.

* Here is a good article explaining why to use **services**: [Why to separate controllers from services](https://www.coreycleary.me/why-should-you-separate-controllers-from-services-in-node-rest-apis/)

### Our Structure
#### Backend --REST API--
For the REST API, there are folders named **routes** and **controllers**, which (pretty obviously) contain routes and controllers for the project. 
Currently there is not a need for middleware, so there is no **middleware** folder. However, one may be added in the future.
It was decided that services would not be necessary, since most of the logic happening in the backend will be fairly straightforward. Any business logic can be 
placed in controllers.
#### Backend --REST API Endpoints--
The REST API for "Fill Order" contains the following endpoints:

* **/fill-order/api/orders**
* **/fill-order/api/orders/{order-id}**
* **/fill-order/api/orders/timestamp/{timestamp}**

The **/orders** endpoint accepts GET and POST requests:
* GET --> Returns list of all filled orders
* POST --> Create a new filled order

The **/orders/{order-id}** endpoint accepts GET and DELETE requests:
* GET --> Returns the filled order with the provided ID
* DELETE --> Deletes the filled order with the provided ID

The **/orders/timestamp/{timestamp}** endpoint accepts GET requests:
* GET --> Returns all orders that were filled on a date past the given timestamp

#### Backend --Database--
The Mongoose library, which is being used to communicate with the project's MongoDB database, utilizes what are called **models** to define schemas in the database.
So at the top level, there is a **db** folder to hold all files related to the database, and inside of this is a **models** folder to hold any necessary models.

## Frontend
### Important Info
#### -- Libraries --
All libraries/packages are installed under the **node_modules** directory. All the following technologies are found under this directory.

Framework: [React](https://reactjs.org/docs/forms.html)

Styling Library: [Material-UI](https://material-ui.com/)

Testing Framework: [Jest](https://jestjs.io/)
 #### -- React Page Structure + Files --
 All files related to the actual page structure of our feature are found under the **src** folder. As of 3/3/20 we have plans to split down directory down into more subdirectories.


