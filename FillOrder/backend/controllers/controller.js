("use strict");

var mongoose = require("mongoose"),
  Order = mongoose.model("Order");

// Removes DeprecationWarning: Mongoose: `findOneAndUpdate()` and `findOneAndDelete()`
// see https://mongoosejs.com/docs/deprecations.html#findandmodify
mongoose.set("useFindAndModify", false);

// GET /orders
exports.listAllOrders = function (req, res) {
  Order.find({}, function (err, order) {
    if (err) res.status(400).send(err);
    else res.status(200).json(order);
  });
};

// POST /orders
exports.createOrder = function (req, res) {
  var newOrder = new Order(req.body);
  newOrder.save(function (err, order) {
    if (err) res.status(400).send(err);
    else {
      res.status(201).json(order);
    }
  });
};

exports.updateOrder = function (req, res) {
  Order.findByIdAndUpdate(req.params.id, req.body, { new: true })
    .then((order) => {
      if (!order) {
        return res
          .status(404)
          .send({ message: "Order not found " + req.params.id });
      }
      res.send(order);
    })
    .catch((err) => {
      if (err.kind === "objectId") {
        return res.status(404).send({
          message: "Order not found with id " + req.params.id,
        });
      }
      return res.status(500).send({
        message: "Error updating order with id " + req.params.id,
      });
    });
};

// GET /orders/:id
exports.getOrderByID = function (req, res) {
  Order.find({ _id: req.params.id }, function (err, order) {
    if (err) res.status(400).send(err);
    else res.status(200).json(order);
  });
};

// DETELE /orders/:id
exports.deleteOrder = function (req, res) {
  Order.deleteOne({ _id: req.params.id }, function (err, order) {
    if (order.deletedCount === 0) {
      res.status(400).json({ order, message: "Order not found" }).send();
    } else {
      if (err) res.status(400).send();
      else res.status(200).json(order).send();
    }
  });
};

// https://docs.mongodb.com/manual/reference/operator/query-comparison/
// GET /orders/timestamp/:ts
exports.allOrdersAfterUnixtime = function (req, res) {
  if (req.params.ts < 0) {
    res.status(400).send("Cannot have negative time");
  } else {
    Order.find({ dateFilled: { $gte: req.params.ts } }, function (err, order) {
      if (err) {
        res.send(400);
      } else {
        res.status(200).send(order);
      }
    });
  }
};

exports.updateOrder = function (req, res) {
  Order.findByIdAndUpdate(req.params.id, req.body, { new: true }, function (
    err,
    order
  ) {
    if (!order) {
      res.status(400).send("Can not find order. ");
    } else {
      if (err) {
        return res.status(500).send(err);
      } else {
        res.send(order);
      }
    }
  });
};
