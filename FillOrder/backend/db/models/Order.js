"use strict";
var mongoose = require("mongoose");
var Schema = mongoose.Schema;
var foodItemSchema = require("./foodItem");

var orderSchema = new Schema(
  {
    _id: Number,
    dateFilled: Number,
    details: {
      foodItems: [foodItemSchema],
    },
  },
  {
    timestamps: false,
    versionKey: false,
  }
);

const Order = mongoose.model("Order", orderSchema);
module.exports = Order;
