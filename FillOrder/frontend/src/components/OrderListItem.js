import React, { Component } from 'react'
import { Button, Avatar} from '@material-ui/core';
import { withStyles} from '@material-ui/core/styles';
import { green } from '@material-ui/core/colors';

export class OrderListItem extends Component {

    render() {
        return (
            <div class="orderListItem" style={orderListItemStyle}>
                <div class="orderLeft" style={orderLeft}>
                <Avatar style={{marginLeft: '10px', marginRight: '70px'}}/>
                <h2>#1123</h2>
                </div>
              
                <h2>4/10/2020</h2>

                <div class="orderRight" style={orderRight}>
                <h2>14 items</h2>
                <FillButton>Fill</FillButton>
                </div>
            </div>
        )
    }
}

const orderLeft = {
    display: 'flex',
    justifyContent: 'space-around',
    lineHeight: '41px'
}

const orderRight = {
    display: 'flex',
    justifyContent: 'space-around',
    lineHeight: '41px'
}

const orderListItemStyle = {
    height: '60px',
    width: '80%',
    minWidth: '700px',
    border: 'rgb(93, 93, 93) 0px 0px 0px 1px',
    borderRadius: '2px',
    boxShadow: '0px 0px 0px 1px #5d5d5d',
    display: 'flex',
    margin: '1em',
    fontSize: '10px',
    alignItems: 'center',
    justifyContent: 'space-between',
}

const FillButton = withStyles({
    root: {
        color: 'white',
        backgroundColor: green[500],
        borderRadius: '7px',
        marginLeft: '70px',
        marginRight: '10px',
        '&:hover': {
            backgroundColor: green[700],
          },
    },
    label: {
        color: 'white',
        textTransform: 'none',
        fontSize: '15px',
    },
})(Button);

export default OrderListItem