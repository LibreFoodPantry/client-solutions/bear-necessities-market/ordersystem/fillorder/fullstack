import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import ListOfOrders from './components/pages/ListOfOrders';

import Header from './components/layout/Header';

import './App.css';


class App extends Component {
  state = {
    orderListItems: []
  }

    render() {
      return (
        <Router>
          <div className="App">
            <div className="container">
              <Header />

              <ListOfOrders />
              
              <Route path="/listoforders" component={ListOfOrders} />
            </div>
          </div>
        </Router>
      )
    }
}
export default App;
