const app = require("../app");
const supertest = require("supertest");
const request = supertest(app);

const mongoose = require("mongoose");
const Order = require("../db/models/Order");
const dbHandler = require("./dbhandler");

beforeAll(async () => await dbHandler.connect());
afterEach(async () => await dbHandler.clearDatabase());
afterAll(async () => await dbHandler.closeDatabase());

describe("GET /orders Test Pass", () => {
  it("Respond with 200 accepted", async (done) => {
    var testOrderObj1 = new Order(testOrder1);
    await testOrderObj1.save();
    const res = await request.get("/orders");
    expect(res.status).toBe(200);
    expect(res.id).toStrictEqual(testOrder1.id);
    expect(res.body[0].details).toStrictEqual(testOrder1.details);
    done();
  });
});

describe("POST /orders with Order", () => {
  it("respond with 201 created", async (done) => {
    const res = await request.post("/orders").send(testOrder1);
    expect(res.status).toBe(201);
    expect(res.body).toStrictEqual(testOrder1);
    done();
  });
});

describe("POST /orders with no Order", () => {
  it("Respond with 400 bad request", async (done) => {
    const res = await request.post("/orders").send();
    expect(res.status).toBe(400, done());
  });
});

describe("UPDATE /orders/:id with order ID", () => {
  it('Responds with 200 "Ok" and Updated Object', async (done) => {
    var testOrderObj1 = new Order(testOrder1);
    await testOrderObj1.save();

    const res = await request.put("/orders/1").send(updateTestOrder1);
    expect(res.status).toBe(200);
    expect(res.body).toStrictEqual(updateTestOrder1, done());
  });
});

describe("UPDATE /orders/:id with in correct ID", () => {
  it("Responds with 404 not found and the JSON", async (done) => {
    var testOrderObj1 = new Order(testOrder1);
    await testOrderObj1.save();

    const res = await request.put("/orders/2").send(updateTestOrder1);
    expect(res.status).toBe(400);
    expect(res.body).toStrictEqual({}, done());
  });
});

describe("POST /orders/timestamp/:ts", function () {
  it("Get orders Filled after UnixTime", async (done) => {
    var testOrderObj1 = new Order(testOrder1);
    var testOrderObj2 = new Order(testOrder2);
    var testOrderObj3 = new Order(testOrder3);
    await testOrderObj1.save();
    await testOrderObj2.save();
    await testOrderObj3.save();

    const res = await request.get("/orders/timestamp/0").send();
    expect(res.status).toBe(200);
    expect(res.body[0]).toStrictEqual(testOrder1);
    expect(res.body[1]).toStrictEqual(testOrder2);
    expect(res.body[2]).toStrictEqual(testOrder3);
    done();
  });
});

describe("POST /orders/timestamp/:ts", () => {
  it("Respond with 400 negative time not allowed", async (done) => {
    request.get("/orders/timestamp/-1").expect(400, done);
  });
});

describe("Given an Order ID get the Order with ID", () => {
  it("Respond with 200 and Order", async (done) => {
    //Add orders to database
    var testOrderObj1 = new Order(testOrder1);
    await testOrderObj1.save();

    const res = await request.get("/orders/1");
    expect(res.status).toBe(200);
    expect(res.body[0]).toStrictEqual(testOrder1, done());
  });
});

describe("Given an Order ID that doesn't exist fail", () => {
  it("Respond with 400 and []", async (done) => {
    //Add orders to database
    var testOrderObj1 = new Order(testOrder1);
    await testOrderObj1.save();

    const res = await request.get("/orders/2");
    expect(res.status).toBe(200);
    expect(res.body).toStrictEqual([], done());
  });
});

describe("Given an Order with ID 1 DELETE order", () => {
  it("Respond with an deletedCount=1 and status 200 ", async (done) => {
    var testOrderObj1 = new Order(testOrder1);
    await testOrderObj1.save();

    const res = await request.delete("/orders/1").send();
    expect(res.status).toBe(200);
    expect(res.body.deletedCount).toBe(1, done());
  });
});

describe("Given an Database with an Order of ID 1", () => {
  it("Respond with 400 and message: Order not Found", async (done) => {
    var testOrderObj1 = new Order(testOrder1);
    await testOrderObj1.save();

    const res = await request.delete("/orders/2").send();
    expect(res.body.message).toBe("Order not found");
    expect(res.body.order.deletedCount).toBe(0);
    expect(res.status).toBe(400, done());
  });
});

const testOrder1 = {
  _id: 1,
  dateFilled: 1,
  details: {
    foodItems: [
      {
        itemName: "Bologna",
        status: "true",
        reason: "In Stock",
      },
      {
        itemName: "Bologna",
        status: "true",
        reason: "In Stock",
      },
    ],
  },
};

const updateTestOrder1 = {
  _id: 1,
  dateFilled: 2,
  details: {
    foodItems: [
      {
        itemName: "Ham",
        status: "true",
        reason: "In Stock",
      },
      {
        itemName: "Cheese",
        status: "false",
        reason: "In Stock",
      },
    ],
  },
};

const testOrder2 = {
  _id: 2,
  dateFilled: 1,
  details: {
    foodItems: [
      {
        itemName: "Bologna",
        status: "true",
        reason: "In Stock",
      },
      {
        itemName: "Bologna",
        status: "true",
        reason: "In Stock",
      },
    ],
  },
};
const testOrder3 = {
  _id: 3,
  dateFilled: 1,
  details: {
    foodItems: [
      {
        itemName: "Bologna",
        status: "true",
        reason: "In Stock",
      },
      {
        itemName: "Bologna",
        status: "true",
        reason: "In Stock",
      },
    ],
  },
};
