## Current Test Coverage

#### GET /orders

**Given:** A user is requesting all orders using GET /orders
**When:** An order already exists
**Then:** Check response status equals 200 and response body matches sent data

#### POST /orders

**Given:** A user is sending a JSON object to POST /orders
**When:** The JSON object is created
**Then:** The response status should be 201 and the res body should match

**Given:** A user is sending an empty JSON to POST /orders
**When:** The API attempts to create the Object
**Then:** The response status should be 400 because the database can't create an empty object

#### GET /orders/timestamp/:ts

**Given:** There exist multiple orders in the MongoDB
**When:** The client POST /orders/timestamp/0
**Then:** All Orders that exist should be returned with a status code of 200

**Given:** A client executes a GET request to /orders/timestamp
**When:** When the request is made a negative number is used
**Then:** A response message returned and an error message of 400

### GET /orders/:id

**Given:** A database with an Order with ID 1
**When:** Client POST /orders/1
**Then:** Receive status 200 and Json Object

**Given:** A database with only an Order with ID 1
**When:** Client POST /orders/2
**Then:** Receive status 400 and an empty array

### DELETE /orders/:id

**Given:** A database with an Order with ID 1
**When:** Client DELETE /orders/1
**Then:** Return status 200 with JSON to compare

**Given:** A database with an Order with ID 1
**When:** Client DELETE /orders/2
**Then:** Return status 400, error message, and Empty Array

### PUT /orders/:id

**Given:** A database with an Order
**When:** Client Updates Object with new Data
**Then:** Status code 200 and new data matches returned data

**Given:** A database with an Order
**When:** Client updates Order with ID that doesnt exists
**Then:** API returns 400 error and empty object
