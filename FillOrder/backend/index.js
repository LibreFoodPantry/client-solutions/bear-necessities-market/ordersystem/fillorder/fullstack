const MongoClient = require("mongodb").MongoClient;
const express = require("express");
const app = express();
const port = 8088;
const cors = require("cors");

app.use(cors());
app.options("*", cors());

app.get("/", (req, res) => {
  //res.send("Hello");
  const client = new MongoClient("mongodb://mongo:27017", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  });
  client.connect(function (err) {
    if (err) throw err;

    const collection = client.db("Database").collection("devices");

    collection.insert({ id: 1, name: "device1" }, function (err, result) {
      collection.find().toArray(function (err, docs) {
        console.log(docs);
        res.send(docs);
        client.close();
      });
    });
  });
});

app.listen(port, "0.0.0.0", () => console.log(`Running on port ${port}!`));
