module.exports = function (app) {
  var control = require("../controllers/controller");

  require("dotenv").config();

  app
    .route(process.env.FO_API_URL + "orders")
    .get(control.listAllOrders)
    .post(control.createOrder);

  app
    .route(process.env.FO_API_URL + "orders/timestamp/:ts")
    .get(control.allOrdersAfterUnixtime);

  app
    .route(process.env.FO_API_URL + "orders/:id")
    .get(control.getOrderByID)
    .delete(control.deleteOrder)
    .put(control.updateOrder);
};
