import React from 'react'
import {Button, Grid, AppBar, Typography} from '@material-ui/core';
import ArrowBackIosIcon from '@material-ui/icons/ArrowBackIos';
import { withStyles } from '@material-ui/core/styles';
import OrderListItem from '../OrderListItem.js';
import OrderListAppBar from '../layout/OrderListAppBar.js';


export default function About() {
    return (
        <React.Fragment>
            <div class="pcontainer" style={pcontainerStyle}>
                <OrderListAppBar></OrderListAppBar>
                <div class="content-top-header" style={contentTopHeader}>
                    <Grid container spacing={24} direction="row" justify="space-evenly">    
                        <h3 style={contentTopLabels}> Order ID </h3>
                        <h3 style={contentTopLabels}> Date Ordered </h3>
                        <h3 style={contentTopLabels}> Number of Items </h3>
                    </Grid>
                </div>


                <div class="content" style={contentStyle}>

                    <Grid container spacing={24} direction="column" alignItems="center" justify="space-between">
                        <OrderListItem/>
                        <OrderListItem/>
                        <OrderListItem/>
                        <OrderListItem/>
                        <OrderListItem/>
                        <OrderListItem/>
                        <OrderListItem/>
                        <OrderListItem/>
                        <OrderListItem/>
                        <OrderListItem/>
                        <OrderListItem/>
                        <OrderListItem/>
                        <OrderListItem/>
                        <OrderListItem/>
                        <OrderListItem/>
                        <OrderListItem/>
                    </Grid>
                    
                </div>
        </div>
        </React.Fragment>
    )
}



const pcontainerStyle = {
    width: '100%',
    height: '100%',
    minWidth: '735px',
    backgroundColor: '#004B8D',
    position: 'fixed',
    overflowY: 'hidden',
}

const contentStyle = {
    background: 'white', 
    height: '100%', 
    minWidth: '735px',
    marginLeft: '20%', 
    marginRight: '20%',
    overflow: 'auto'  // added scroll bar here
    
}

const contentTop = {
    marginLeft: '20%', 
    marginRight: '20%',
    minWidth: '735px',
    height: '50px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    lineHeight: '50px',
    background: '#FDB813',
}

const contentTopHeader = {
    marginLeft: '20%', 
    marginRight: '20%',
    minWidth: '735px',
    height: '50px',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    lineHeight: '50px',
    background: 'rgb(56, 56, 56)'
}

const contentTopLabels = {
    color:'white'
}
